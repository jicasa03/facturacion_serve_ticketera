const express = require('express');
const socketIO=require('socket.io');
const path = require('path');
const {Usuarios}=require('../class/usuarios');
const {Impresoras}=require('../class/impresoras');

const app = express();
const http= require('http');
const { emit } = require('process');
const publicPath = path.resolve(__dirname, '../public');
const port = process.env.PORT || 8080;
let server=http.createServer(app);
app.use(express.static(publicPath));

// io=esta esta comunicciaocn del backend
let io=socketIO(server);

//const usuarios=new Usuarios();
const impresoras=new Impresoras();


io.on('connection',(client)=>{
  client.on('iniciar_impresora',(usuario)=>{
    //console.log(usuario);
    client.join(usuario.ruc);
    let impre=impresoras.agregarImpresora(client.id,usuario.ruc,usuario.nombre_impresora,usuario.usuario);
    client.broadcast.to(usuario.ruc).emit('listaimpresora',impre);
  //  let per=impresoras.agregarPersona(client.id,usuario.id_usuario,usuario.idpedido);
   console.log(impre);
     
  });

  client.on('iniciar_cliente',(usuario)=>{
   console.log(usuario.ruc);
    client.join(usuario.ruc);
    ///  console.log(impresoras.mostrarImpresora());
    // console.log(impresoras.getlistaImpresora(usuario.ruc));
   let lista= impresoras.getlistaImpresora(usuario.ruc);
   io.sockets.in(usuario.ruc).emit('listaimpresora',lista);

    //client.emit('listaimpresora',lista);
   // io.to(usuario.ruc).emit('listaimpresora',lista
      
     // console.log(lista);
   // client.broadcast.to(usuario.ruc).emit('listaimpresora',lista);
  });

  /*  client.on('iniciar',(usuario)=>{
      console.log(usuario);
        client.join(usuario.idpedido);
      let per=usuarios.agregarPersona(client.id,usuario.id_usuario,usuario.idpedido);
      console.log(per);
       
       let res={};
       res["cantidad"]=usuarios.cantidad();
       console.log(res);
      client.broadcast.emit('cantidad',res);
    });*/

    client.on("imprimir",(resp)=>{
      client.broadcast.emit('imprimir',resp);
    });




    client.on('disconnect',()=>{
      
     let envio= impresoras.borrarImpresora(client.id);
     console.log(envio);
       let res={};
       if(typeof(envio) != 'undefined'){

        client.broadcast.to(envio.ruc).emit('listaimpresora',impresoras.getlistaImpresora(envio.ruc));
        
       }
     //  res["cantidad"]=usuarios.cantidad();
      // console.log(res);
      // client.broadcast.emit('cantidad',res);
      //  console.log(per);
      });

    /*  client.on('ubicacion',(req)=>{
        let usuario=usuarios.getPersonas(client.id);
        console.log(req);
        client.broadcast.to(usuario.idpedido).emit('ubicacion',req);
      });*/
});
server.listen(port, (err) => {

    if (err) throw new Error(err);

    console.log(`Servidor corriendo en puerto ${ port }`);

});